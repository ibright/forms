ko.bindingHandlers['validate'] = {
  init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    var value = ko.unwrap(valueAccessor());
    if (!viewModel.errors) {
      ko.validation.group(viewModel, {observable: true});
    }

    function extendBinding(binding) {
      var observable = allBindingsAccessor.get(binding);
      if (observable && ko.isObservable(observable)) {
        observable.extend({validatable: true});
        observable.extend(value);
      }
    }

    extendBinding('text');
    extendBinding('value');
  }
};