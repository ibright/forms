## validate-binding

to use validation, first you need to import the libraries

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/knockout-min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/knockout.validation.min.js"></script>
    <script type="text/javascript" src="js/validate-binding.js"></script>

then in the html, the elements that are to be validated must bew wrapped in an element

    <div class="form-group" data-bind="validationElement: myProperty">
      <label class="control-label">Test</label>
      <input class="form-control" type="text"
             data-bind="value: myProperty, validate: {required: true, pattern: '.+'}"
             placeholder="Enter something"/>
    </div>

in the javascript before you go and do anything with the data, check it for errors

    if (model.errors().length !== 0) {
      model.errors.showAllMessages();
      return;
    }
