#Default Forms

This folder contains some default forms for use by simple, standard workflows. They have been designed (in cases) to be multi-purpose, to cope with different use cases and scenarios by the provision of configuration parameters as part of the form URL. This configuration requires ibright Drive >= 1.3.512.

##view-details.html

This is a generic form for viewing the step details of a job. It will display the `siteName`, `address` and `timeWindow` of the step along with listing all the fields provided in the `customFields.display` object of both the step and job. The provided fields will be displayed as string key/value pairs without extra formatting.

e.g.
```
{
    ...
    "customFields": {
        "foo": "bar",
        "display": {
            "Contact": "Joe Bloggs",
            "Phone": "555-543-0234"
        }
    }
}
```

##set-run-vehicle.html

This form will request that a run is assigned to the vehicle associated with the device on which the form is filled out. It is intended to be used as an ad-hoc form (i.e. not part of a job). A route number will be requested (and optionally a day) and the form will produce a runId, and other fields required to assign the run to the vehicle.

####Configuration

Configuration options can be provided by specifying URL query parameters as part of the form URL.

**regex** *regex* optional, default=`^\d+$` - specifies the validation regex pattern for the route number.

**showDate** *boolean* optional, default=`true` - specifies if a field should be shown for the driver to select whether the route number specified is for yesterday, today, or tomorrow. Even if set to true, this selection will only be visible before 6am or after 6pm.

**idFormat** optional, default=`${date}|${route}` - enable the conversion of entered route number and date to be combined with other characters to form the runId. `${date}` will be replaced with the current, or selected date, formatted YYYY-MM-DD. `${route}` will be replaced with the text entered by the driver.

**alreadySet** *boolean* optional, default=`false` - if true, will transfer run from another already assigned vehicle; NOTE: will fail if run is not already assigned.

##transfer-run-vehicle.html

This form will request that an already assigned run is transferred from another vehicle to the vehicle associated with the device on which the form is filled out. It is intended to be used as an ad-hoc form (i.e. not part of a job).

####Configuration

Configuration options can be provided by specifying URL query parameters as part of the form URL.

**regex** *regex* optional, default=`^\d+$` - specifies the validation regex pattern for the route number.

**showDate** *boolean* optional, default=`true` - specifies if a field should be shown for the driver to select whether the route number specified is for yesterday, today, or tomorrow. Even if set to true, this selection will only be visible before 6am or after 6pm.

**idFormat** optional, default=`${date}|${route}` - enable the conversion of entered route number and date to be combined with other characters to form the runId. `${date}` will be replaced with the current, or selected date, formatted YYYY-MM-DD. `${route}` will be replaced with the text entered by the driver.

##clear-run-vehicle.html
This form will request that a run is unassigned from the vehicle associated with the device on which the form is filled out. It is intended to be used either as an ad-hoc form (i.e. not part of a job) or as a job step action.

####Configuration

Configuration options can be provided by specifying URL query parameters as part of the form URL.

**regex** *regex* optional, default=`^\d+$` - specifies the validation regex pattern for the route number.

**showDate** *boolean* optional, default=`true` - specifies if a field should be shown for the driver to select whether the route number specified is for yesterday, today, or tomorrow. 

**idFormat** optional, default=`${date}|${route}` - enable the conversion of entered route number and date to be combined with other characters to form the runId. `${date}` will be replaced with the current, or selected date, formatted YYYY-MM-DD. `${route}` will be replaced with the text entered by the driver.

**forceRemote** *boolean* optional, default=`false` - if true, will unassign the run, even if it is not assigned to the vehicle associated with the device on which the form is filled out.

**reason** *regex* optional - if set, presents text input for reason, validated by provided regular expression. This reason will be included in the capture.

**reasons** *string (comma separated list)* optional - if set, presents a drop-down selection for reason populated with given values. The selected reason will be included in the capture. Note: not to be used with **reason**.
