ko.bindingHandlers.title = {
  update: function(element, valueAccessor) {
    var value = ko.utils.unwrapObservable(valueAccessor());
    $(element).prop('title', value);
  }
};